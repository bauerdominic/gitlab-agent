load("@io_bazel_rules_go//go:def.bzl", "go_library")
load("cmd.bzl", "push_bundle")

prefix = "{STABLE_CI_REGISTRY}/{STABLE_CONTAINER_REPOSITORY_PATH}/"

TAG = {
    prefix + "kas:{STABLE_BUILD_GIT_TAG}": "//cmd/kas:container",
    prefix + "kas:{STABLE_BUILD_GIT_TAG}-race": "//cmd/kas:container_race",
    prefix + "agentk:{STABLE_BUILD_GIT_TAG}-amd64": "//cmd/agentk:container",
    prefix + "agentk:{STABLE_BUILD_GIT_TAG}-arm": "//cmd/agentk:container_arm",
    prefix + "agentk:{STABLE_BUILD_GIT_TAG}-arm64": "//cmd/agentk:container_arm64",
    prefix + "agentk:{STABLE_BUILD_GIT_TAG}-amd64-race": "//cmd/agentk:container_race",
    prefix + "agentk:{STABLE_BUILD_GIT_TAG}-arm-race": "//cmd/agentk:container_arm_race",
    prefix + "agentk:{STABLE_BUILD_GIT_TAG}-arm64-race": "//cmd/agentk:container_arm64_race",
    prefix + "cli:{STABLE_BUILD_GIT_TAG}": "//cmd/cli:container",
}

STABLE = {
    prefix + "kas:stable": "//cmd/kas:container",
    prefix + "kas:stable-race": "//cmd/kas:container_race",
    prefix + "agentk:stable-amd64": "//cmd/agentk:container",
    prefix + "agentk:stable-arm": "//cmd/agentk:container_arm",
    prefix + "agentk:stable-arm64": "//cmd/agentk:container_arm64",
    prefix + "agentk:stable-amd64-race": "//cmd/agentk:container_race",
    prefix + "agentk:stable-arm-race": "//cmd/agentk:container_arm_race",
    prefix + "agentk:stable-arm64-race": "//cmd/agentk:container_arm64_race",
    prefix + "cli:stable": "//cmd/cli:container",
}

TAG_AND_STABLE = {}

TAG_AND_STABLE.update(TAG)

TAG_AND_STABLE.update(STABLE)

push_bundle(
    name = "push-commit",
    images = {
        prefix + "kas:{STABLE_BUILD_GIT_COMMIT}": "//cmd/kas:container",
        prefix + "kas:{STABLE_BUILD_GIT_COMMIT}-race": "//cmd/kas:container_race",
        prefix + "agentk:{STABLE_BUILD_GIT_COMMIT}-amd64": "//cmd/agentk:container",
        prefix + "agentk:{STABLE_BUILD_GIT_COMMIT}-arm": "//cmd/agentk:container_arm",
        prefix + "agentk:{STABLE_BUILD_GIT_COMMIT}-arm64": "//cmd/agentk:container_arm64",
        prefix + "agentk:{STABLE_BUILD_GIT_COMMIT}-amd64-race": "//cmd/agentk:container_race",
        prefix + "agentk:{STABLE_BUILD_GIT_COMMIT}-arm-race": "//cmd/agentk:container_arm_race",
        prefix + "agentk:{STABLE_BUILD_GIT_COMMIT}-arm64-race": "//cmd/agentk:container_arm64_race",
        prefix + "cli:{STABLE_BUILD_GIT_COMMIT}": "//cmd/cli:container",
    },
)

push_bundle(
    name = "push-latest",
    images = {
        prefix + "kas:latest": "//cmd/kas:container",
        prefix + "kas:latest-race": "//cmd/kas:container_race",
        prefix + "agentk:latest-amd64": "//cmd/agentk:container",
        prefix + "agentk:latest-arm": "//cmd/agentk:container_arm",
        prefix + "agentk:latest-arm64": "//cmd/agentk:container_arm64",
        prefix + "agentk:latest-amd64-race": "//cmd/agentk:container_race",
        prefix + "agentk:latest-arm-race": "//cmd/agentk:container_arm_race",
        prefix + "agentk:latest-arm64-race": "//cmd/agentk:container_arm64_race",
        prefix + "cli:latest": "//cmd/cli:container",
    },
)

push_bundle(
    name = "push-tag",
    images = TAG,
)

push_bundle(
    name = "push-tag-and-stable",
    images = TAG_AND_STABLE,
)

go_library(
    name = "cmd",
    srcs = [
        "build_info.go",
        "utils.go",
    ],
    importpath = "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v14/cmd",
    visibility = [
        "//cmd:__subpackages__",
    ],
    deps = ["@com_github_spf13_cobra//:cobra"],
)
