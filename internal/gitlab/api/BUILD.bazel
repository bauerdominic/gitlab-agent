load("@io_bazel_rules_go//go:def.bzl", "go_library")
load("//build:build.bzl", "go_custom_test")
load("//build:proto.bzl", "go_proto_generate")

go_proto_generate(
    src = "api.proto",
    workspace_relative_target_directory = "internal/gitlab/api",
    deps = [
        "//pkg/agentcfg:proto",
        "@com_github_envoyproxy_protoc_gen_validate//validate:validate_proto",
    ],
)

go_library(
    name = "api",
    srcs = [
        "api.pb.go",
        "api.pb.validate.go",
        "get_agent_info.go",
        "get_allowed_agents.go",
        "get_project_info.go",
        "helper.go",
        "module_request.go",
        "post_agent_configuration.go",
        "send_usage_ping.go",
    ],
    importpath = "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v14/internal/gitlab/api",
    visibility = ["//:__subpackages__"],
    deps = [
        "//internal/api",
        "//internal/gitlab",
        "//internal/tool/prototool",
        "//pkg/agentcfg",
        "@com_github_envoyproxy_protoc_gen_validate//validate:go_custom_library",
        "@org_golang_google_protobuf//reflect/protoreflect",
        "@org_golang_google_protobuf//runtime/protoimpl",
        "@org_golang_google_protobuf//types/known/anypb",
    ],
)

go_custom_test(
    name = "api_test",
    srcs = [
        "get_agent_info_test.go",
        "get_project_info_test.go",
        "post_agent_configuration_test.go",
        "validation_test.go",
    ],
    embed = [":api"],
    deps = [
        "//internal/gitlab",
        "//internal/tool/prototool",
        "//internal/tool/testing/mock_gitlab",
        "//internal/tool/testing/testhelpers",
        "//pkg/agentcfg",
        "@com_github_google_go_cmp//cmp",
        "@com_github_stretchr_testify//assert",
        "@com_github_stretchr_testify//require",
        "@org_golang_google_protobuf//testing/protocmp",
    ],
)
