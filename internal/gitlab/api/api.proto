syntax = "proto3";

// If you make any changes make sure you run: make regenerate-proto

package gitlab.agent.gitlab.api;

option go_package = "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v14/internal/gitlab/api";

import "pkg/agentcfg/agentcfg.proto";
import "validate/validate.proto";

// Configuration contains shared fields from agentcfg.CiAccessProjectCF and agentcfg.CiAccessGroupCF.
// It is used to parse response from the allowed_agents API endpoint.
// See https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/kubernetes_ci_access.md#apiv4joballowed_agents-api.
message Configuration {
  string default_namespace = 1 [json_name = "default_namespace"];
  agentcfg.CiAccessAsCF access_as = 2 [json_name = "access_as"];
}

message AllowedAgent {
  int64 id = 1 [json_name = "id"];
  ConfigProject config_project = 2 [json_name = "config_project", (validate.rules).message.required = true];
  Configuration configuration = 3 [json_name = "configuration"];
}

message ConfigProject {
  int64 id = 1 [json_name = "id"];
}

message Pipeline {
  int64 id = 1 [json_name = "id"];
}

message Project {
  int64 id = 1 [json_name = "id"];
  repeated Group groups = 2 [json_name = "groups"];
}

message Group {
  int64 id = 1 [json_name = "id"];
}

message Job {
  int64 id = 1 [json_name = "id"];
}

message User {
  int64 id = 1 [json_name = "id"];
  string username = 2 [json_name = "username", (validate.rules).string.min_bytes = 1];
}

message Environment {
  string slug = 1 [json_name = "slug", (validate.rules).string.min_bytes = 1];
}

message AllowedAgentsForJob {
  repeated AllowedAgent allowed_agents = 1 [json_name = "allowed_agents"];
  Job job = 2 [json_name = "job", (validate.rules).message.required = true];
  Pipeline pipeline = 3 [json_name = "pipeline", (validate.rules).message.required = true];
  Project project = 4 [json_name = "project", (validate.rules).message.required = true];
  User user = 5 [json_name = "user", (validate.rules).message.required = true];
  Environment environment = 6 [json_name = "environment"]; // optional
}
