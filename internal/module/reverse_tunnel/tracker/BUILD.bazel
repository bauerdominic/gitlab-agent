load("//build:build.bzl", "go_custom_test")
load("@io_bazel_rules_go//go:def.bzl", "go_library")
load("//build:proto.bzl", "go_proto_generate")

go_proto_generate(
    src = "tracker.proto",
    workspace_relative_target_directory = "internal/module/reverse_tunnel/tracker",
    deps = [
        "//internal/module/reverse_tunnel/info:proto",
        "@com_github_envoyproxy_protoc_gen_validate//validate:validate_proto",
    ],
)

go_library(
    name = "tracker",
    srcs = [
        "tracker.extra_methods.go",
        "tracker.go",
        "tracker.pb.go",
        "tracker.pb.validate.go",
    ],
    importpath = "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v14/internal/module/reverse_tunnel/tracker",
    visibility = ["//:__subpackages__"],
    deps = [
        "//internal/module/reverse_tunnel/info",
        "//internal/tool/logz",
        "//internal/tool/redistool",
        "@com_github_envoyproxy_protoc_gen_validate//validate:go_custom_library",
        "@com_github_go_redis_redis_v8//:redis",
        "@org_golang_google_protobuf//reflect/protoreflect",
        "@org_golang_google_protobuf//runtime/protoimpl",
        "@org_golang_google_protobuf//types/known/anypb",
        "@org_uber_go_zap//:zap",
    ],
)

go_custom_test(
    name = "tracker_test",
    srcs = [
        "tracker_test.go",
        "validation_test.go",
    ],
    embed = [":tracker"],
    deps = [
        "//internal/module/reverse_tunnel/info",
        "//internal/tool/redistool",
        "//internal/tool/testing/mock_redis",
        "//internal/tool/testing/testhelpers",
        "@com_github_golang_mock//gomock",
        "@com_github_google_go_cmp//cmp",
        "@com_github_stretchr_testify//assert",
        "@com_github_stretchr_testify//require",
        "@org_golang_google_protobuf//proto",
        "@org_golang_google_protobuf//testing/protocmp",
        "@org_golang_google_protobuf//types/known/anypb",
        "@org_golang_google_protobuf//types/known/timestamppb",
        "@org_uber_go_zap//zaptest",
    ],
)
